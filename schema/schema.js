const { gql } = require("apollo-server-express");

const typeDefs = gql`
  type Post {
    id: ID
    title: String!
    content: String
    published: Boolean
    author: User
  }

  type User {
    id: ID
    email: String!
    name: String
    posts: [Post]
    profile: Profile
  }

  type Profile {
    id: ID
    bio: String
    user: User
  }

  #Root type
  type Query {
    users: [User]
    user(id: ID!): User
    posts: [Post]
    post(id: ID!): Post
    profiles: [Profile]
    profile(id: ID!): Profile
  }

  type Mutation {
    createUser(email: String!, name: String): User
    createPost(
      title: String!
      authorId: ID!
      content: String
      published: Boolean
    ): Post
    createProfile(bio: String, userId: ID!): Profile
    updateProfile(id: ID!, bio: String): Profile
    deleteUser(id: ID!): Boolean
  }
`;

module.exports = typeDefs;
