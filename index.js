const express = require("express");
const { ApolloServer } = require("apollo-server-express");

const typeDefs = require("./schema/schema");
const resolvers = require("./resolver/resolver");

const server = new ApolloServer({
  typeDefs,
  resolvers,
  csrfPrevention: true,
  cache: "bounded",
  formatError: (err) => {
    if (err.message.startsWith("Database Error: ")) {
      return new Error("Internal server error");
    }

    return err;
  },
});
const app = express();
async function startServer() {
  await server.start();
  server.applyMiddleware({ app });
}
startServer();

app.listen({ port: 4000 }, () => {
  console.log(`Server ready at http://localhost:4000${server.graphqlPath}`);
});
// async function main() {
//     --CREATE
//     await prisma.user.create({
//       data: {
//         name: "Alice",
//         email: "alice@prisma.io",
//         posts: {
//           create: { title: "Hello World" },
//         },
//         profile: {
//           create: { bio: "I like turtles" },
//         },
//       },
//     });
//     --GET ALL
//     const allUsers = await prisma.user.findMany({
//       include: {
//         posts: true,
//         profile: true,
//       },
//     });
//     console.dir(allUsers, { depth: null });
//     const user = await prisma.user.findFirst({
//       where: {
//         id: 1,
//       },
//     });
//     console.log(user);
//     --UPDATE
//     const post = await prisma.post.update({
//       where: { id: 1 },
//       data: { published: true },
//     });
//     console.log(post);
// }

// main()
//   .catch((e) => {
//     throw e;
//   })
//   .finally(async () => {
//     await prisma.$disconnect();
//   });
