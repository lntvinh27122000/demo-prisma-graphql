const { PrismaClient, Prisma } = require("@prisma/client");

const prisma = new PrismaClient();
const resolvers = {
  Query: {
    users: async () => {
      const allUsers = await prisma.user.findMany({
        include: {
          posts: true,
          profile: true,
        },
      });
      console.dir(allUsers, { depth: null });
      return allUsers;
    },

    user: async (parent, args) => {
      const user = await prisma.user.findUnique({
        where: {
          id: parseInt(args.id.toString()),
        },
      });
      return user;
    },

    posts: async () => {
      const allPosts = await prisma.post.findMany({
        include: {
          author: true,
        },
      });
      console.dir(allPosts, { depth: null });
      return allPosts;
    },
    post: async (parent, args) => {
      const post = await prisma.post.findUnique({
        where: {
          id: parseInt(args.id.toString()),
        },
      });
      return post;
    },
    profiles: async () => {
      const allProfiles = await prisma.profile.findMany({
        include: {
          user: true,
        },
      });
      console.dir(allProfiles, { depth: null });
      return allProfiles;
    },
    profile: async (parent, args) => {
      const profile = await prisma.profile.findUnique({
        where: {
          id: parseInt(args.id.toString()),
        },
      });
      return user;
    },
  },

  Mutation: {
    createUser: async (parent, args) => {
      try {
        console.log(args);
        const newUser = await prisma.user.create({
          data: {
            email: args.email,
            name: args.name,
          },
        });
        return newUser;
      } catch (error) {
        throw new Error("Error when create user");
      }
    },
    createPost: async (parent, args) => {
      try {
        const newPost = await prisma.post.create({
          data: {
            title: args.title,
            authorId: parseInt(args.authorId),
            content: args.content,
            published: args.published,
          },
        });
        return newPost;
      } catch (error) {
        throw new Error("Error when create post");
      }
    },
    createProfile: async (parent, args) => {
      try {
        console.log(args);
        const newProfile = await prisma.profile.create({
          data: {
            bio: args.bio,
            userId: parseInt(args.userId),
          },
        });
        return newProfile;
      } catch (error) {
        throw error;
      }
    },

    updateProfile: async (parent, args) => {
      try {
        const profile = await prisma.profile.update({
          where: { id: parseInt(args.id) },
          data: { bio: args.bio },
        });
        return profile;
      } catch (error) {
        throw error;
      }
    },
    deleteUser: async (parent, args) => {
      try {
        const deletePosts = prisma.post.deleteMany({
          where: {
            authorId: parseInt(args.id),
          },
        });
        const deleteUser = prisma.user.delete({
          where: {
            id: parseInt(args.id),
          },
        });
        const transaction = await prisma.$transaction([
          deletePosts,
          deleteUser,
        ]);

        return true;
      } catch (error) {
        return false;
      }
    },
  },

  Post: {
    author: async (parent, args) => {
      const user = await prisma.user.findUnique({
        where: {
          id: parseInt(parent.authorId.toString()),
        },
      });
      return user;
    },
  },

  User: {
    posts: async (parent, args) => {
      console.log(parent);
      const posts = await prisma.post.findMany({
        where: {
          authorId: parseInt(parent.id.toString()),
        },
      });
      return posts;
    },

    profile: async (parent, args) => {
      const profile = await prisma.profile.findUnique({
        where: {
          userId: parseInt(parent.id.toString()),
        },
      });
    },
  },

  Profile: {
    user: async (parent, args) => {
      const user = await prisma.user.findUnique({
        where: {
          id: parseInt(parent.authorId?.toString()),
        },
      });
      return user;
    },
  },
};

module.exports = resolvers;
